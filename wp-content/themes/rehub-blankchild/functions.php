<?php

add_action( 'wp_enqueue_scripts', 'enqueue_parent_theme_style' );
function enqueue_parent_theme_style() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
	if (is_rtl()) {
		 wp_enqueue_style( 'parent-rtl', get_template_directory_uri().'/rtl.css', array(), RH_MAIN_THEME_VERSION);
	}     
	wp_enqueue_script( "sp-custom-scripts", get_stylesheet_directory_uri() . '/assets/js/custom.js' , array(), true  );
}

// Register Top menu
if ( ! function_exists( 'ss_register_nav_menu' ) ) {
 
    function ss_register_nav_menu(){
        register_nav_menus( array(
            'header_menu' => __( 'Header Menu', 'rehub-theme' ),
        ) );
    }
    add_action( 'after_setup_theme', 'ss_register_nav_menu', 0 );
}

?>