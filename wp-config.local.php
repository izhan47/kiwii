<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'kiwi' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '85|6mf]sxrrd,0eI_N^H=80d&hx}9l7Xm ~B>s*qcblS!=M/%?wtqh}]sb/XLMk]' );
define( 'SECURE_AUTH_KEY',  '/q}eMv&34#|PC6&vi-ZfUe~7&>adOm%KSy702YjB[-o@B1N0)GpjCs((at`rne$@' );
define( 'LOGGED_IN_KEY',    'gblxT4mS2<:hj|d4#I=7wPLb/:w=uAaW*zXK(:U+aeOuNcG#tu;[MQkFj=k,VFll' );
define( 'NONCE_KEY',        '&QbY}l*m]({$$QLeDF7@J*BOrnm@a+)#=g<)cc<[M+soN`SE2vtDbLL]k&H4zUsi' );
define( 'AUTH_SALT',        'dLQn6}vFk|jv9k0(F){y4ZYFa30UOT/C&AB/>/%%O0F`/L|$(G-tD.rx==ZC`T7S' );
define( 'SECURE_AUTH_SALT', 'NR7tL<8#W:pj^S{;k@L!|J&Ounx@;S0$AT1jlgEK4Fw&R3+$fwX|DU7ruBLJ=`v%' );
define( 'LOGGED_IN_SALT',   '?q;[JSbT{qW:Bp^=R-04)l=0?p o>a`8hz0[Ih$BNRsMa=AilAM=(NavY~B0Lbeg' );
define( 'NONCE_SALT',       'ec[^:a~SMmiigDjs^{0}}4z yO*E(p%Q<zH|FNJi(;c6om[MtPt)oP.uC9^_<Bhv' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';   



